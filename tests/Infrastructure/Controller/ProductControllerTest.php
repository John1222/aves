<?php
namespace App\Tests\Infrastructure\Controller;

use App\Infrastructure\Entity\Product;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProductControllerTest extends WebTestCase
{
    private static KernelBrowser $client;
    private static EntityManager $em;
    
    public static function setUpBeforeClass()
    {
        self::$client = static::createClient();
        self::$em = self::$client->getContainer()->get( 'doctrine' )->getManager();
    }

    public function tearDown(): void
    {
        self::$em->createQuery( 'DELETE ' . Product::class . ' p' )->execute();
    }

    public function testProductCreation()
    {
        $productCode = 'PRD_1';
        $productTitle = 'Продукт 1';

        $response = $this->request( json_encode( [
            'title' => $productTitle,
            'code'  => $productCode
        ] ) );
        $this->assertArrayHasKey( 'id', $response );

        /** @var Product $productFound */
        $productFound = self::$em->getRepository( Product::class )->find( $response[ 'id' ] );
        
        $this->assertNotNull( $productFound );
        $this->assertEquals( 200, self::$client->getResponse()->getStatusCode() );
        $this->assertEquals( $productTitle, $productFound->getTitle() );
        $this->assertEquals( $productCode, $productFound->getCode() );
    }

    public function testFailureOnCreatingWithDuplicatedCode()
    {
        for( $requestIdx = 0; $requestIdx < 2; ++$requestIdx ) {
            $response = $this->request( json_encode( [
                'title' => 'Продукт 1',
                'code'  => 'PRD_1'
            ] ) );
        }

        $this->assertEquals( 400, self::$client->getResponse()->getStatusCode() );
        $this->assertEquals( 'Product with this code already exists', $response[ 'message' ] );
    }

    public function testFailureOnRequestWithInvalidJSONFormat()
    {
        $response = $this->request( '{ 123' );
        $this->assertEquals( 400, self::$client->getResponse()->getStatusCode() );
        $this->assertEquals( 'Invalid JSON format', $response[ 'message' ] );
    }

    private function request( string $json ): array
    {
        self::$client->request( 'POST', '/api/products', [], [], [], $json );
        return json_decode( self::$client->getResponse()->getContent(), true );
    }
}
