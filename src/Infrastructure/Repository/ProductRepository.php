<?php

namespace App\Infrastructure\Repository;

use App\Infrastructure\Entity\Product;
use Doctrine\ORM\EntityRepository;

/**
 * Репозиторий для доменной модели продукта
 */
class ProductRepository extends EntityRepository
{
    public function save(Product $product)
    {
        $this->getEntityManager()->persist( $product );
        $this->getEntityManager()->flush();
    }
}
