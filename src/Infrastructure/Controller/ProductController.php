<?php
namespace App\Infrastructure\Controller;

use App\Exception\ApiException;
use App\Infrastructure\Entity\Product;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @Route("/api/products", name="create_product")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function create( Request $request ): JsonResponse
    {
        try {
            $json = json_decode( $request->getContent(), true, 512, JSON_THROW_ON_ERROR );
            $product = new Product( $json[ 'title' ], $json[ 'code' ] );
            $this->getDoctrine()->getRepository( Product::class )->save( $product );
            return new JsonResponse( [ 'id' => $product->getId() ] );
        }
        catch( \JsonException $e ) {
            throw new ApiException( 'Invalid JSON format' );
        }
        catch( UniqueConstraintViolationException $e ) {
            throw new ApiException( 'Product with this code already exists' );
        }
        catch( \Throwable $e ) {
            throw new ApiException( 'Unknown error' );
        }
    }
}
