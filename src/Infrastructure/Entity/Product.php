<?php

namespace App\Infrastructure\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Entity(repositoryClass="App\Infrastructure\Repository\ProductRepository")
 * @ORM\Table(name="`products`", uniqueConstraints={
 *     @UniqueConstraint(name="idx_unq_code", columns={"code"})
 * })
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private int $id;

    /**
     * Наименование товара.
     *
     * @ORM\Column(type="string")
     */
    private string $title;

    /**
     * Артикул.
     *
     * @ORM\Column(type="string")
     */
    private string $code;

    public function __construct(string $title, string $code)
    {
        $this->title = $title;
        $this->code = $code;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getCode(): string
    {
        return $this->code;
    }
}
